<?php

namespace App\Http\Controllers;

use App\Models\zoom;
use App\Models\rental;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class rentalController extends Controller
{
    public function index()
    {
        $rental= rental
            ::join('zoom', 'rental.zoom_id', '=', 'zoom.id')
            ->select('zoom.*', 'rental.*')
            ->get();


        return view('rental.rentList', ['rental_data' => $rental]);
    }

    public function create()
    {
        $zoom = zoom::all();
        return view('rental.createRent', ['zoom' => $zoom]);
    }

    public function insert(Request $request)
    {
        $request->validate([
            'zoom_id' => 'required',
            'activity' => 'required',
            'desc' => 'required',
            'rent_date' => 'required',
            'back_date' => 'required',
        ]);


        // CEK TANGGAL AKUN ZOOM
        $rent_date = rental::where('zoom_id', $request->zoom_id)->whereBetween('rent_date', [$request->rent_date, $request->back_date])->first();

        // KONDISI JIKA JADWAL SUDAH TERDAFTAR
        if ($rent_date != null) {
            // MASUKKAN ULANG JADWAL
            return redirect('/createRent')->with('gagal', 'Jadwal Tidak Tersedia!');
        }

        $rental = new rental();
        $rental->zoom_id = $request->zoom_id;
        $rental->activity = $request->activity;
        $rental->desc = $request->desc;
        $rental->rent_date = ($request->rent_date);
        $rental->back_date = ($request->back_date);

        $rental->save();

        return redirect('/rentView')->with('sukses', 'Data berhasil tersimpan!');
    }

    public function edit($id)
    {
        $rental = rental::where('id', $id)->first();

        $data = [
            'edit' => $rental,
        ];

        return view('rental.editRent', $data);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'note' => 'required',
        ]);

        $rental = rental::find($id);
        $rental->status = $request->status;
        $rental->note = $request->note;

        $rental->save();

        if ($rental)
            return redirect('/rentView')->with('sukses', 'Status berhasil diedit!');
        else
            return redirect('/rentView')->with('gagal', 'Status gagal diedit!');;
    }

    public function delete($id)
    {
        $rental = rental::find($id);
        $status = rental
            ::select('rental.*')
            ->where('rental.id', $id)
            ->where('rental.status', 'Approved')
            ->count();

        if ($rental != null) {
            if ($status) {
                return redirect('/rentView')->with('gagal', 'Status Peminjaman Approved, Tidak dapat menghapus request peminjaman');
            } else {
                $rental->delete();
                return redirect('/rentView')->with('sukses', 'Data berhasil dihapus!');;
            }
        }
    }
}
