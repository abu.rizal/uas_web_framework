<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\rental;
use App\Models\zoom;

class ScheduleController extends Controller
{
    public function index()
    {

        $rental = rental
            ::join('zoom', 'rental.zoom_id', '=', 'zoom.id')
            ->select('zoom.*', 'rental.*')
            ->get();

        return view('schedule.schedule', ['schedule_data' => $rental]);
    }

    public function detail($id)
    {
        $rental = rental::where('rental.id', $id)
            ->join('zoom', 'rental.zoom_id', '=', 'zoom.id')
            ->select('zoom.*', 'rental.*')
            ->first();


        $data = [
            'edit' => $rental,
        ];

        return view('schedule.detail', $data);
    }
}
