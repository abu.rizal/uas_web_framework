<?php

namespace App\Http\Controllers;

use App\Models\zoom;

use Illuminate\Http\Request;

class zoomController extends Controller
{
    public function index()
    {
        $zoom = zoom::all(); // menampilkan data tampa data yang di soft delete
        //$zoom = zoom::withTrashed()->get(); // menampilkan data yang sudah di soft delete
        return view('zoom.zoomList', ['data_zoom' => $zoom]);
    }

    public function create()
    {
        return view('zoom.zoomCreate');
    }

    public function insert(Request $request)
    {
        $zoom = new zoom();
        $zoom->account_name = $request->account_name;
        $zoom->capacity = $request->capacity;
        $zoom->account_status = $request->account_status;

        $zoom->save();

        if ($zoom)
            return redirect('/zoomList')->with('sukses', 'Data berhasil tersimpan');
        else
            return redirect('/zoomList')->with('gagal', 'Data gagal disimpan');
    }

    public function edit($id)
    {
        $zoom = zoom::where('id', $id)->first();

        $data = [
            'edit' => $zoom
        ];

        return view('zoom.zoomEdit', $data);
    }

    public function update(Request $request, $id)
    {
        $zoom = zoom::find($id);
        $zoom->account_name = $request->account_name;
        $zoom->capacity = $request->capacity;
        $zoom->account_status = $request->account_status;

        $zoom->save();

        if ($zoom)
            return redirect('/zoomList')->with('sukses', 'Data berhasil diedit');
        else
            return redirect('/zoomList')->with('gagal', 'Data gagal diedit');
    }

    public function delete($id)
    {
        $zoom = zoom::find($id);
        if ($zoom != null) {
            $zoom->delete();
            // $zoom->forcedelete(); // data tetap terhapus walaupun menggunakan soft delete

            if ($zoom)
                return redirect('/zoomList')->with('sukses', 'Data berhasil terhapus');
            else
                return redirect('/zoomList')->with('gagal', 'Data gagal dihapus');
        }
    }
}
