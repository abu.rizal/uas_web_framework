<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>GetZoom | Register</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/5.1/examples/sign-in/">

    <link href="{{ asset("/css/bootstrap.min.css") }}" rel="stylesheet">
    <link href="{{ asset("/css/signin.css") }}" rel="stylesheet">

</head>
<body class="text-center">
    <div class="container" style="display: flex; justify-content: center; align-items: center;">
        <div class="card col-sm-5">
            <main class="form-signin">
                <form action="\register" method="POST" >
				@csrf
				<input class=" form-control" type="hidden" placeholder="Nama" name="is_admin" required value="0">

                <img class="mb-4 user-avatar rounded-circle border" src='{{ asset("/images/logo2.png") }}' alt="" width="100" height="100">
                <h1 class="h3 mb-3 fw-normal">Create Account</h1>

				<div class="form-floating">
					<label for="floatingInput">Fullname</label>
                    <input type="text" name="name" class="@error('name') is-invalid @enderror form-control" id="floatingInput" placeholder="Fullname" required value="{{ old('name') }}">
                    @error('name')
						<div class="form-control-feedback">
							<span style="color:red">
								{{ $message }}
							</span>
						</div>
					@enderror
                </div>

				<div class="form-floating">
					<label for="floatingInput">Username</label>
                    <input type="text" name="username" class="@error('username') is-invalid @enderror form-control" id="floatingInput" placeholder="Fullname" required value="{{ old('username') }}">
                    @error('username')
						<div class="form-control-feedback">
							<span style="color:red">
								{{ $message }}
							</span>
						</div>
					@enderror
                </div>

                <div class="form-floating">
					<label for="floatingInput">Email address</label>
                    <input type="email" name="email" class="@error('email') is-invalid @enderror form-control" id="floatingInput" placeholder="E-mail" required value="{{ old('email') }}">
                    @error('email')
						<div class="form-control-feedback">
							<span style="color:red">
								{{ $message }}
							</span>
						</div>
					@enderror
                </div>

                <div class="form-floating">
					<label for="floatingPassword">Password</label>
                    <input type="password" name="password" class="@error('password') is-invalid @enderror form-control" id="floatingPassword" placeholder="Password" required>
                    @error('password')
						<div class="form-control-feedback">
							<span style="color:red">
								{{ $message }}
							</span>
						</div>
					@enderror
                </div>

                <div class="form-floating">
					<label for="floatingPassword">Confirm Password</label>
                    <input type="password" name="password"  class="@error('password') is-invalid @enderror form-control" id="floatingPassword" placeholder="Password" required>
                    @error('password')
						<div class="form-control-feedback">
							<span style="color:red">
								{{ $message }}
							</span>
						</div>
					@enderror
                </div>
        
                
                <button class="w-100 btn btn-lg btn-primary" type="submit">Create</button>
				<p>Are you a staff? <a href="{{ url("/staffRegister") }}">Register here</a></p>
                <p>Already have account? <a href="{{ url("/login") }}">Login here</a></p>
                </form>
            </main>
        </div>
    </div>
        
        
            
</body>
</html>