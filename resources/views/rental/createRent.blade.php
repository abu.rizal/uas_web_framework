@extends('../componnents/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}" type="text/javascript"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
@endsection

@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            @if (session('gagal'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                    {{ session('gagal') }}
                </div>
            @endif
            <div class="m-subheader">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <h3 class="m-subheader__title text-uppercase m-subheader__title--separator">
                            Data Peminjaman
                        </h3>
                    </div>
                </div>
            </div>
        </div>

        <form method="post"
            action="{{ route('insertRent') }}"
            class="form-send m-form m-form--fit m-form--label-align-right"
            enctype="multipart/form-data">

            {{ csrf_field() }}

            <div class="m-content">
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                    Request Peminjaman
                                </h3>
                            </div>
                        </div>
                    </div>

                    <div class="m-portlet__body">
                        <div class="form-group m-form__group">
                            <label>Nama Akun Zoom Aktif</label>
                            {{-- <input type="text" name="id_zoom" class="@error('id_zoom') is-invalid @enderror form-control m-input" required value="{{ old('id_zoom') }}"> --}}
                            <select name="zoom_id" class="form-control m-input">
                                    <option value="">Pilih Akun Zoom</option>
                                        @foreach ($zoom as $row)
                                            @if ($row->account_status == ('Active'))
                                                <option value="{{ $row->id }}">{{ $row->account_name.' - '.$row->capacity. ' orang '}}</option>
                                            @endif
                                        @endforeach

                            </select>
                            @error('zoom_id')
                                <div class="form-control-feedback">
                                    <span style="color:red">
                                        {{ $message }}
                                    </span>
                                </div>
                            @enderror
                        </div>
                        <div class="form-group m-form__group">
                            <label>Nama Kegiatan</label>
                            <input type="text" name="activity" class="@error('activity') is-invalid @enderror form-control m-input" required value="{{ old('activity') }}">
                            @error('activity')
                                <div class="form-control-feedback">
                                    <span style="color:red">
                                        {{ $message }}
                                    </span>
                                </div>
                            @enderror
                        </div>
                        <div class="form-group m-form__group">
                            <label>Deskripsi</label>
                            <textarea name="desc" value="{{ old('desc') }}" class="@error('desc') is-invalid @enderror form-control m-input" required value="{{ old('desc') }}"></textarea>
                            @error('desc')
                                <div class="form-control-feedback">
                                    <span style="color:red">
                                        {{ $message }}
                                    </span>
                                </div>
                            @enderror
                        </div>
                        <div class="form-group m-form__group">
                            <label>Tanggal Pinjam</label>
                            <input type="datetime-local" name="rent_date" class="@error('rent_date') is-invalid @enderror form-control m-input" required value="{{ old('rent_date') }}">
                            @error('rent_date')
                                <div class="form-control-feedback">
                                    <span style="color:red">
                                        {{ $message }}
                                    </span>
                                </div>
                            @enderror
                        </div>
                        <div class="form-group m-form__group">
                            <label>Tanggal Kembali</label>
                            <input type="datetime-local" name="back_date" class="@error('back_date') is-invalid @enderror form-control m-input" required value="{{ old('back_date') }}">
                            @error('back_date')
                                <div class="form-control-feedback">
                                    <span style="color:red">
                                        {{ $message }}
                                    </span>
                                </div>
                            @enderror
                        </div>
                    </div>
                    <div class="m-portlet akses-list">
                        <div class="m-portlet__foot m-portlet__foot--fit">
                            <div class="m-form__actions text-center">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                                <a href="{{ route('rentList') }}" class="btn btn-secondary">Batal</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  
        </form>
    </div>
@endsection