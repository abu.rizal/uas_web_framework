@extends('../componnents/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}" type="text/javascript"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
@endsection

@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            @if (session('gagal'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                    {{ session('gagal') }}
                </div>
            @elseif (session('sukses'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                    {{ session('sukses') }}
                </div>
            @endif
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        Daftar Peminjaman
                    </h3>
                </div>
                @if(Auth()->user()->is_admin == 0)
                    <a href="{{ route('createRent') }}" class="btn btn-success">Request</a>
                @endif
            </div>
        </div>
        <div class="m-content">
            <div class="m-portlet akses-list">
                <div class="m-portlet__body">
                    <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                        <div class="row align-items-center">
                            <div class="col-xl-8 order-2 order-xl-1">
                                <div class="form-group m-form__group row align-items-center">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="m_datatable" id="local_data">
                        <div class="table-responsive">
                            <table class="akses-list table table-bordered">
                                <thead>
                                    <tr>
                                        <th width="20">No</th>
                                        <th width="20">Nama Akun Zoom</th>
                                        <th width="20">Nama Kegiatan</th>
                                        <th width="20">Deskripsi</th>
                                        <th width="20">Tanggal Pinjam</th>
                                        <th width="20">Tanggal Kembali</th>
                                        <th width="20">Durasi</th>
                                        <th width="20">Status</th>
                                        <th width="20">Catatan</th>
                                        <th width="20">Menu</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($rental_data as $data)
                                        @php
                                            // $durasi = strtotime($data->tanggal_kembali) - strtotime($data->tanggal_pinjam);
                                            // $jam = $durasi / 3600 % 24;
                                            // $menit = $durasi / 60 % 60; 
                                            $rent = new DateTime($data->rent_date);
                                            $back = new DateTime($data->back_date);
                                        @endphp
                                    
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $data->account_name }}</td>
                                            <td>{{ $data->activity }}</td>
                                            <td>{{ $data->desc }}</td>
                                            <td>{{ $data->rent_date }}</td>
                                            <td>{{ $data->back_date }}</td>
                                            <td>{{ $rent->diff($back)->format("%d Hari, %h Jam and %i Menit") }}  </td>
                                            <td>
                                                @if($data->status != null)
                                                    @if ($data->status == ('Rejected'))
                                                        <button class="btn m-btn--pill btn-danger btn-sm m-btn m-btn--custom">
                                                        {{ $data->status }}
                                                    </button>
                                                    @elseif ($data->status == ('Cancelled'))
                                                        <button class="btn m-btn--pill btn-warning btn-sm m-btn m-btn--custom">
                                                            {{ $data->status }}
                                                        </button>
                                                    @elseif ($data->status == ('Finished'))
                                                        <button class="btn m-btn--pill btn-primary btn-sm m-btn m-btn--custom">
                                                            {{ $data->status }}
                                                        </button>
                                                    @else
                                                        <button class="btn m-btn--pill btn-primary btn-sm m-btn m-btn--custom">
                                                            {{ $data->status }}
                                                        </button>
                                                    @endif
                                                @endif
                                            </td>
                                            <td>{{ $data->note }}</td>
                                            <td>
                                                <div class="btn-group m-btn-group" role="group" aria-label="...">
                                                    @if(Auth()->user()->is_admin == 1)
                                                        <a href="/editRent/{{ $data->id }}" class="btn btn-success">
                                                            Edit
                                                        </a>
                                                    @endif
                                                    &nbsp;&nbsp;
                                                    @if(Auth()->user()->is_admin == 1)
                                                        <a href="/deleteRent/{{ $data->id }}" class="btn btn-danger">
                                                            Hapus
                                                        </a>
                                                    @endif
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection