@extends('../componnents/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}" type="text/javascript"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
@endsection

@section('content')
<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="d-flex justify-content-center">
            
            <div class="col-md-4">
                <div class="card">
                    
                    <div class="card-body">
                        <div class="mx-auto d-block">
                            <a href="{{ route('zoomList') }}">
                                <img class="rounded-circle mx-auto d-block" src="{{ asset("/images/logo2.png") }}" alt="Card image cap" style="max-width: 200px; max-height: 200px">
                            </a>
                            <h5 class="text-sm-center mt-2 mb-1">Zoom</h5>
                
                        </div>
                    </div>
                </div>
            </div>	
			
            
            <div class="col-md-4">
                <div class="card">
                
                    <div class="card-body">
                        <div class="mx-auto d-block">
                            <a href="{{ route('scheduleList') }}">
                                <img class="rounded-circle mx-auto d-block" src="{{ asset("/images/calendar.png") }}" alt="Card image cap" style="max-width: 200px; max-height: 200px">
                            </a>
                            <h5 class="text-sm-center mt-2 mb-1">Jadwal</h5>
                
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                
                    <div class="card-body">
                        <div class="mx-auto d-block">
                            <a href="{{ route('rentList') }}">
                                <img class="rounded-circle mx-auto d-block" src="{{ asset("/images/rent.png") }}" alt="Card image cap" style="max-width: 200px; max-height: 200px">
                            </a>
                            <h5 class="text-sm-center mt-2 mb-1">Peminjaman</h5>
                
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>
@endsection
