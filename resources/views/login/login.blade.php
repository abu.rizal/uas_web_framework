<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>GetZoom | Login</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/5.1/examples/sign-in/">

    <link href="{{ asset("/css/bootstrap.min.css") }}" rel="stylesheet">
    <link href="{{ asset("/css/signin.css") }}" rel="stylesheet">

</head>

<body class="text-center">
	
		
    <div class="container" style="max-width: 1080px;">
		<div class="content mt-3">
        
			@if (session()->has('success'))
								
				<div class="alert alert-success">
					{{ session('success') }}
				</div>
			
			@endif
	
			@if (session()->has('loginError'))
								
				<div class="alert alert-danger">
					{{ session('loginError') }}
				</div>
			
			@endif
        <div class="row" style="display: flex; justify-content: center; align-items: center;">
            <div class="col title-div">
				<!-- <a href='https://pngtree.com/so/meetings'>meetings png from pngtree.com/</a> -->
                <img src="{{ asset("/images/meet.png") }}" class="img-fluid rounded-start" alt="login_images">
            </div>
            <div class="col-sm-5">
                <div class="card">
                    <main class="form-signin">
						
                        <form action="{{ route('login') }}" method="post">
							@csrf
                        <img class="mb-4 user-avatar rounded-circle border" src='{{ asset("/images/logo2.png") }}' alt="" width="100" height="100">
                        <h1 class="h3 mb-3 fw-normal">Login to GetZoom</h1>
                    
                        <div class="form-floating">
							<label for="floatingInput">Email address</label>
                            <input type="email" class="form-control" id="floatingInput" placeholder="E-mail" name="email" required value="{{ old('email') }}">
                            
                        </div>
                        <div class="form-floating">
							<label for="floatingPassword">Password</label>
                            <input type="password" class="form-control" id="floatingPassword" placeholder="Password" name="password" required>
                            
                        </div>
                        <p><a href="#">Forgot Password?</a></p>
                    
                        <button class="w-100 btn btn-lg btn-primary" type="submit">Login</button>
                        <p>Don't have any acoount? <a href="{{ url("/register") }}">Register here</a></p>
                        </form>
                    </main>
                </div>
            </div>
        </div>
    </div>
        
        
            
</body>
</html>