@extends('../componnents/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}" type="text/javascript"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
@endsection

@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <form method="post"
            action="{{ route('updateRent', ['id' => $edit->id]) }}"
            class="form-send m-form m-form--fit m-form--label-align-right"
            enctype="multipart/form-data">

            {{ csrf_field() }}

            <div class="m-content">
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                    Detail Jadwal
                                </h3>
                            </div>
                        </div>
                    </div>

                    <div class="m-portlet__body">
                        <div class="form-group m-form__group">
                            <label>Nama Akun</label>
                            <input type="text" name="account_name" value="{{ $edit->account_name }}" class="form-control m-input" disabled>
                        </div>
                        <div class="form-group m-form__group">
                            <label>Nama Kegiatan</label>
                            <input type="text" name="activity" value="{{ $edit->activity }}" class="form-control m-input " disabled>
                        </div>
                        <div class="form-group m-form__group">
                            <label>capasity</label>
                            <input type="text" name="capasity" value="{{ $edit->capasity }}" class="form-control m-input" disabled>
                        </div>
                        <div class="form-group m-form__group">
                            <label>desc</label>
                            <input type="text" name="desc" value="{{ $edit->desc }}" class="form-control m-input" disabled>
                        </div>
                        <div class="form-group m-form__group">
                            <label>Tanggal Pinjam</label>
                            <input type="text" name="rent_date" value="{{ $edit->rent_date }}" class="form-control m-input" disabled>
                        </div>
                        <div class="form-group m-form__group">
                            <label>Tanggal Kembali</label>
                            <input type="text" name="back_date" value="{{ $edit->back_date }}" class="form-control m-input" disabled>
                        </div>
                        <div class="form-group m-form__group">
                            <label>Link Zoom</label>
                            <input type="text" name="note" value="{{ $edit->note }}" class="form-control m-input" disabled>
                        </div>
                    </div>
                </div>
            </div>  
        </form>
    </div>
@endsection